# project-generator
Generates a C++ project from the specified template.

# Usage
This project contains templates for creating TNT Coders C++ projects. To generate a project from
the available templates use the `generate_project.py` script.

Currently only the "library" type is supported but more options will be added later.

```
python generate_project.py --type=<type>
```

Provide a name and description for the project when prompted:

```
Name: <name>
Description: <description>
```

A project will be generated in the `output` directory in a folder named `<name>`.
