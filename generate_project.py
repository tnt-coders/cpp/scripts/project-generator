import argparse, fileinput, os, re, shutil, sys

# Replaces keywords in the provided string
def replace_keywords(text):
    text = text.replace("@NAME@", name.upper())
    text = text.replace("@Name@", name[0].upper() + name[1:])
    text = text.replace("@name@", name.lower())
    text = text.replace("@description@", description)
    return text

# Copies a template file and replaces keywords within the file
def copy_template_file(source, destination):

    try:
        file = open(source, "r")
        contents = file.read()
        dest_contents = replace_keywords(contents)

        destination = open(destination, "w")
        destination.write(dest_contents)
    except:

        # Will throw an exception for binary files (just copy them as is)
        shutil.copy2(source, destination)

    return

# Parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--type', type=str)
args = parser.parse_args()

# Validate input arguments
if not args.type:
    sys.exit("Missing required input argument '--type'")

# For now library is the only valid type, but we will add more later
valid = False
valid_types = ["library", "juce-plugin"]
for valid_type in valid_types:
    if args.type == valid_type:
        valid = True

if not valid:
    sys.exit("Invalid option provided for argument '--type'")

# Get the name and description of the project
name = input("Name: ")
description = input("Description: ")

# Create the root directory
root = "output" + os.sep + name
os.makedirs(root)

# Generate project files
print("Generating project files...")
for subdir, dirs, files in os.walk("templates" + os.sep + args.type):
    dest_subdir = subdir.replace("templates" + os.sep + args.type, "")
    dest_subdir = replace_keywords(dest_subdir)

    # Process directories
    for dir in dirs:
        dest_dir = replace_keywords(dir)
        print(root + dest_subdir + os.sep + dest_dir)
        os.makedirs(root + dest_subdir + os.sep + dest_dir)

    # Process files
    for file in files:
        dest_file = replace_keywords(file)
        print(root + dest_subdir + os.sep + dest_file)
        copy_template_file(subdir + os.sep + file, root + dest_subdir + os.sep + dest_file)

# Flush stdout before issuing system commands
sys.stdout.flush()

# Initialize a git repo for the project
os.chdir(root)
os.system("git init")
os.system("git submodule add https://gitlab.com/tnt-coders/cpp/cmake/cmake.git")
os.system("git submodule set-branch --branch v3.2.0 cmake")
os.system("git add --all")
os.system("git commit -m \"Initial commit\"");
os.system("git tag v0.0.0")
